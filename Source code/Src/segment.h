/* ========================================
 *
 * Copyright Niek Ilmer, 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Niek Ilmer.
 *
 * ========================================
*/

#include <stdint.h>
#include "stm32f0xx_hal.h"

void segment_task(void const * args);
typedef struct led{
    uint8_t green;
    uint8_t red;
    uint8_t blue;
}led;


typedef struct digit{ //RGB for every segment
 led d[6];
 led c[6]; 
 led g[6];
 led b[6];
 led a[6];
 led f[6];  
 led e[6];
}digit;

typedef struct segment_init{
	digit *digitarray;
	SPI_HandleTypeDef *hspi1;
	DMA_HandleTypeDef *hdma_spi1_tx;
	
}segment_init;

void setSegmentColour(led *segment, uint8_t red, uint8_t green, uint8_t blue);
void setDigit(digit *target, uint8_t number, uint8_t red, uint8_t green, uint8_t blue);

/* [] END OF FILE */

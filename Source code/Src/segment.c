
/* ========================================
 *
 * Copyright Niek Ilmer, 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Niek Ilmer.
 *
 * ========================================
*/

#include "segment.h"
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "stm32f0xx_hal.h"
#include "main.h"
SPI_HandleTypeDef extern hspi1;
RTC_HandleTypeDef extern hrtc;
	uint8_t SPI_digits[sizeof(digit) * 6 * 4 + 2];
	digit digits[6];
int volatile test = sizeof(digit);
#define BRIGHTNESS 8
void segment_task(void const * args){
	SPI_HandleTypeDef *spi = &hspi1;
	//DMA_HandleTypeDef *DMA = ((segment_init*) args)->hdma_spi1_tx;
	uint8_t zeros[100];
	for (int i = 0; i < 100; i++){
		zeros[i] = 0;
	}
	
	int count = 0;
	RTC_TimeTypeDef time;
	time.Hours = 17;
	time.Minutes = 15;
	time.Seconds = 30;
	//HAL_RTC_SetTime(&hrtc, &time, RTC_FORMAT_BIN);
	for(;;){
		HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
		setDigit(&digits[0],time.Hours   / 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
		setDigit(&digits[1],time.Hours   % 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
		setDigit(&digits[2],time.Minutes / 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
		setDigit(&digits[3],time.Minutes % 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
		setDigit(&digits[4],time.Seconds / 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
		setDigit(&digits[5],time.Seconds % 10,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
//		setDigit(&digits[0],count,BRIGHTNESS,BRIGHTNESS,BRIGHTNESS);
//		setDigit(&digits[1],count,0,0,BRIGHTNESS);
//		setDigit(&digits[2],count,BRIGHTNESS,0,0);
//		setDigit(&digits[3],count,0,BRIGHTNESS,0);
//		setDigit(&digits[4],count,BRIGHTNESS,BRIGHTNESS,0);
//		setDigit(&digits[5],count,BRIGHTNESS,0,BRIGHTNESS);
		
		
		
//		while(HAL_GPIO_ReadPin(Power_GPIO_Port, Power_Pin)){
//			osDelay(1000);			
//		}
		count = count >= 9? 0 : count + 1;
		uint8_t *digit_byte = (uint8_t *)digits;
		int index = 1;
		for(int i = 0; i < sizeof(digit)*6; i++){
			SPI_digits[index]    = digit_byte[i] & 0x80? 0xE0 : 0x80;
			SPI_digits[index++] |= digit_byte[i] & 0x40? 0xE  : 0x8;
			SPI_digits[index]    = digit_byte[i] & 0x20? 0xE0 : 0x80;
			SPI_digits[index++] |= digit_byte[i] & 0x10? 0xE  : 0x8;
			SPI_digits[index]    = digit_byte[i] & 0x8?  0xE0 : 0x80;
			SPI_digits[index++] |= digit_byte[i] & 0x4?  0xE  : 0x8;
			SPI_digits[index]    = digit_byte[i] & 0x2?  0xE0 : 0x80;
			SPI_digits[index++] |= digit_byte[i] & 0x1?  0xE  : 0x8;
		}
		SPI_digits[0] = 0;
		HAL_SPI_Transmit(spi,zeros, 100, 100);
		HAL_SPI_Transmit(spi,SPI_digits, 4*4, 100);
		HAL_SPI_Transmit(spi,zeros, 100, 100);
		HAL_SPI_Transmit_DMA(spi,SPI_digits, sizeof(digit)*6*4 + 1);
		osDelay(1000);
	}
}

void setSegmentColour(led* segment, uint8_t red, uint8_t green, uint8_t blue){
    for(uint8_t i = 0; i < 6; i++){
        segment[i].red   = red;
        segment[i].green = green;
        segment[i].blue  = blue;
    }
}

void clearDigit(digit *target){
    for (int i = 0; i < 6*7*3; i++){
        ((uint8_t *) target)[i] = 0;
    }
}

void setDigit(digit *target, uint8_t number, uint8_t red, uint8_t green, uint8_t blue){
    clearDigit(target);
    if(number != 1 && number != 4){
       setSegmentColour(target->a, red, green, blue);
    }
    if(number != 5 && number != 6){
       setSegmentColour(target->b, red, green, blue);
    }
    if(number != 2){
       setSegmentColour(target->c, red, green, blue);
    }
    if(number != 1 && number != 4 && number != 7){
       setSegmentColour(target->d, red, green, blue);
    }
    if(number == 0 || number == 2 || number == 6 || number == 8){
       setSegmentColour(target->e, red, green, blue);
    }
    if(number != 1 && number != 2 && number != 3 && number != 7){
       setSegmentColour(target->f, red, green, blue);
    }
    if(number != 0 && number != 1 && number != 7){
       setSegmentColour(target->g, red, green, blue);
    }
}


/* [] END OF FILE */

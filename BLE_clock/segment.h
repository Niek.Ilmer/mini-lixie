/*=======================================
 *
 * Copyright Niek Ilmer, 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Niek Ilmer.
 *
 * ========================================
*/

#include <stdint.h>
#include <FastLED.h>

#define LED_PIN 22 //signal pin 
#define NUM_LEDS 42*6 //number of LEDs in your strip
#define BRIGHTNESS 254  //brightness  (max 254) 
#define LED_BRIGHTNESS 15
#define LED_TYPE WS2813  // I know we are using ws2812, but it's ok!
#define COLOR_ORDER GRB

void segment_task(void);
void addleds(void);
void boot_Phase(uint8_t number, uint8_t red, uint8_t green, uint8_t blue);


typedef struct digit{ //RGB for every segment
 CRGB e[6];
 CRGB f[6];
 CRGB a[6];
 CRGB b[6];
 CRGB g[6];
 CRGB c[6];
 CRGB d[6];   
}digit;


void setSegmentColour(CRGB *segment, uint8_t red, uint8_t green, uint8_t blue);
void setDigit(digit *target, uint8_t number, uint8_t red, uint8_t green, uint8_t blue);

/* [] END OF FILE */

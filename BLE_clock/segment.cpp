
/* ========================================
 *
 * Copyright Niek Ilmer, 2018
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF Niek Ilmer.
 *
 * ========================================
*/

#include "segment.h"
#include <FastLED.h>
digit digits[6];
int volatile test = sizeof(digit);
void segment_task(){
	int count = 0;
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
	setDigit(&digits[0],timeinfo.tm_hour / 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
	setDigit(&digits[1],timeinfo.tm_hour % 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
	setDigit(&digits[2],timeinfo.tm_min  / 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
	setDigit(&digits[3],timeinfo.tm_min  % 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
	setDigit(&digits[4],timeinfo.tm_sec  / 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
	setDigit(&digits[5],timeinfo.tm_sec  % 10,LED_BRIGHTNESS,LED_BRIGHTNESS,LED_BRIGHTNESS);
  FastLED.show();
}

void setSegmentColour(CRGB* segment, uint8_t red, uint8_t green, uint8_t blue){
    for(uint8_t i = 0; i < 6; i++){
        segment[i].red   = red;
        segment[i].green = green;
        segment[i].blue  = blue;
    }
}

void addleds(){
   FastLED.addLeds<LED_TYPE, 22>((CRGB*)digits, NUM_LEDS);
}

void clearDigit(digit *target){
    for (int i = 0; i < 6*7*3; i++){
        ((uint8_t *) target)[i] = 0;
    }
}

void boot_Phase(uint8_t number, uint8_t red, uint8_t green, uint8_t blue){
  for(int i = 0; i < 6; i++){
    setDigit(&digits[i],number,red,green,blue);    
  }
  FastLED.show();
}

void setDigit(digit *target, uint8_t number, uint8_t red, uint8_t green, uint8_t blue){
    clearDigit(target);
    if(number != 1 && number != 4){
       setSegmentColour(target->a, red, green, blue);
    }
    if(number != 5 && number != 6){
       setSegmentColour(target->b, red, green, blue);
    }
    if(number != 2){
       setSegmentColour(target->c, red, green, blue);
    }
    if(number != 1 && number != 4 && number != 7){
       setSegmentColour(target->d, red, green, blue);
    }
    if(number == 0 || number == 2 || number == 6 || number == 8){
       setSegmentColour(target->e, red, green, blue);
    }
    if(number != 1 && number != 2 && number != 3 && number != 7){
       setSegmentColour(target->f, red, green, blue);
    }
    if(number != 0 && number != 1 && number != 7){
       setSegmentColour(target->g, red, green, blue);
    }
}


/* [] END OF FILE */

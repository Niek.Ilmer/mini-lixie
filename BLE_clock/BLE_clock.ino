/*
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleWrite.cpp
    Ported to Arduino ESP32 by Evandro Copercini
*/

#include <BLEDevice.h>
#include <FastLED.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <EEPROM.h>
#include <WiFi.h>
#include "time.h"
#include "segment.h"



// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "2a4c6cb3-aec2-4e60-92a1-4c63f48b658b"
#define SSID_UUID           "a17bafb4-346b-4a1f-93ba-090b24801767"
#define PASSWORD_UUID       "7a8895c5-9e62-4fa6-9564-8b9cd02bebe3"
#define OFFSET_UUID         "0722f5c7-6499-433d-9312-fe99af91d0ea"

std::string ap_ssid;
std::string ap_password;
std::string offset_string;

const char* ntpServer = "pool.ntp.org";
volatile long  offset = 0;

std::string read_ssid_from_memory(){
  std::string ret;
  for(char i = 0; i < 32; i++){
    if(EEPROM.read(i) != 0){
    Serial.print(EEPROM.read(i));
      ret.push_back(EEPROM.read(i));    
    } else {
      ret.push_back(0); 
      i = 32;
    }
  }
  return ret;
}

void read_offset_from_memory(){
  char ret[8] = {};
  Serial.println("Reading Offset ");
  for(char i = 96; i < 96+8; i++){
      Serial.print(EEPROM.read(i));
      ret[i - 96] = (EEPROM.read(i)); 
  }
  offset = atoi(ret);
  Serial.println();
  Serial.println(offset);
}

bool printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return false;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  return true;
}

void write_ssid_to_memory(std::string ssid){
  for(int i = 0; i < ssid.length(); i++){
    EEPROM.write(i, ssid.c_str()[i]);
  }  
  for(int i = ssid.length(); i < 32; i++){
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
}

void write_offset_to_memory(){
  char str[34] = {0};
  sprintf(str,"%d",offset);
  Serial.println(str);
  for(int i = 0; i < 34; i++){
    EEPROM.write(i + 96, str[i]);
  }  
  EEPROM.commit();
}

std::string read_password_from_memory(){
  std::string ret;
  for(char i = 32; i < 96; i++){
    if(EEPROM.read(i) != 0){
    Serial.print(EEPROM.read(i));
      ret.push_back(EEPROM.read(i));    
    } else {
      ret.push_back(0); 
      i = 200;
    }
  }
  return ret;
}


void write_password_to_memory(std::string password){
  for(int i = 32; i < password.length() + 32; i++){
    EEPROM.write(i, password.c_str()[i-32]);
  }  
  for(int i = password.length() + 32; i < 96; i++){
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
}

class ssid_callback: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) {
        ap_ssid = value;
        write_ssid_to_memory(ap_ssid);
      }
    }
};

class password_callback: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) {
        ap_password = value;
        write_password_to_memory(ap_password);
      }
    }
};

class offset_callback: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();
      Serial.print(value.c_str());
      if (value.length() > 0) {
        offset = atoi(value.c_str());
        write_offset_to_memory();
      }
    }
};

void setup() {
  Serial.begin(115200);
  EEPROM.begin(512);
  //read_offset_from_memory();
  //offset = 3600;
  //write_offset_to_memory();
  addleds();
  boot_Phase(0, LED_BRIGHTNESS, 0, LED_BRIGHTNESS);

  Serial.println("Booted");

  BLEDevice::init("BLE Clock");
  BLEServer *pServer = BLEDevice::createServer();

  BLEService *pService = pServer->createService(SERVICE_UUID);

  BLECharacteristic *ssid_characteristic = pService->createCharacteristic(
                                         SSID_UUID,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  ssid_characteristic->setCallbacks(new ssid_callback());
  
  BLECharacteristic *password_characteristic = pService->createCharacteristic(
                                         PASSWORD_UUID,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  password_characteristic->setCallbacks(new password_callback());
  
  BLECharacteristic *offset_characteristic = pService->createCharacteristic(
                                         OFFSET_UUID,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  offset_characteristic->setCallbacks(new offset_callback());


  Serial.println("Reading EEPROM");
  ap_ssid = read_ssid_from_memory();
  Serial.println("Read ssid");
  ap_password = read_password_from_memory();
  Serial.println("Read password");
  Serial.println(ap_ssid.c_str());
  Serial.println(ap_password.c_str());
  
  read_offset_from_memory();
  //offset = 3600;
  //write_offset_to_memory();
  
  pService->start();
  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->start();
  boot_Phase(1, 0, 0, LED_BRIGHTNESS);
  
  Serial.printf("Connecting to %s ", ap_ssid.c_str());
  WiFi.begin(ap_ssid.c_str(), ap_password.c_str());
  int tries = 0;
  while (WiFi.status() != WL_CONNECTED && tries < 10) {
      delay(500);
      tries++;
      Serial.print(".");
  }
  if(WiFi.status() != WL_CONNECTED){
    boot_Phase(2, LED_BRIGHTNESS, 0, 0);
    Serial.println(" Could not connect");
    while(1){      
    }
  }
  boot_Phase(2, 0, LED_BRIGHTNESS, 0);
  Serial.println(" CONNECTED");
  
  //init and get the time
  configTime(offset, 0, ntpServer);
  if(printLocalTime()){
    boot_Phase(3, 0, LED_BRIGHTNESS, 0);    
  } else {
    boot_Phase(3, LED_BRIGHTNESS, 0, 0);    
  }

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
}

void loop() {
  // put your main code here, to run repeatedly:
  printLocalTime();
  segment_task();
  delay(700);
}

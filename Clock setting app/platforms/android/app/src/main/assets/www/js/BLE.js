


var connectedDevice;

function stringToBytes(string) {
   var array = new Uint8Array(string.length);
   for (var i = 0, l = string.length; i < l; i++) {
       array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}

function connectCallback(device){
	console.log("Connected");
	console.log(JSON.stringify(device));
	var parentElement = document.getElementById('deviceready');
	var deviceElement = parentElement.querySelector('.device');
	deviceElement.textContent = "Connected to BLE clock";
	connectedDevice = device;
	var element = document.querySelector('.ssidclass');
	element.setAttribute('style', 'display:block;');
	element = document.querySelector('.passwordclass');
	element.setAttribute('style', 'display:block;');
}

function disconnectCallback(device){
	console.log("Disconnected");
	connectedDevice = NULL;   
	var parentElement = document.getElementById('deviceready');
	var deviceElement = parentElement.querySelector('.device');
	deviceElement.textContent = "Disconnected from BLE clock";
	var element = document.querySelector('.ssidclass');
	element.setAttribute('style', 'display:none;');
	element = document.querySelector('.passwordclass');
	element.setAttribute('style', 'display:none;');
}
	console.log(JSON.stringify(device));

function setPassword(){
	if (connectedDevice) {
		var element = document.getElementById('password');
		console.log(element.value);
		ble.write(connectedDevice.id, "2a4c6cb3-aec2-4e60-92a1-4c63f48b658b", "7a8895c5-9e62-4fa6-9564-8b9cd02bebe3", stringToBytes(element.value), 
			function() { console.log("Write password success"); },
			function() { console.log("Write password failed"); });
	}
}

function setSsid(){
	if (connectedDevice) {
		var element = document.getElementById('ssid');
		console.log(element.value);
		ble.write(connectedDevice.id, "2a4c6cb3-aec2-4e60-92a1-4c63f48b658b", "a17bafb4-346b-4a1f-93ba-090b24801767", stringToBytes(element.value), 
			function() { console.log("Write SSID success"); },
			function(err) { console.log("Write SSID failed");
							console.log(JSON.stringify(err)); 
						}
					);
	}
}
